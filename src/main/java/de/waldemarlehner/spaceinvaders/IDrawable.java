package de.waldemarlehner.spaceinvaders;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;

public interface IDrawable
{
    void draw( PApplet applet, int xOffset, int yOffset );

    void draw( PGraphics applet, int xOffset, int yOffset );
}
