package de.waldemarlehner.spaceinvaders;

import de.waldemarlehner.javaListenerUtils.Func;
import de.waldemarlehner.javaListenerUtils.Tuple;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;

import java.awt.*;
import java.util.Random;

public class SpaceInvader implements IDrawable
{
    private enum TileType {
        Primary, Secondary, Empty
    }

    private final int xCount;
    private final int yCount;
    private final int tileSize;
    private final int primaryColor;
    private final int secondaryColor;
    private final int seed;
    private final Func.OneArg<Float, TileType> tileTypeDecidingFunction;

    public SpaceInvader( int emptyWeight, int primaryWeight, int secondaryWeight, Color primary, Color secondary, int xCount, int yCount, int seed, int tileSize ) {
        this.seed = seed;
        float weightSum = emptyWeight + primaryWeight + secondaryWeight;
        float primaryToSecondaryBound = primaryWeight / weightSum;
        float secondaryToEmptyWeightBound = primaryToSecondaryBound + secondaryWeight / weightSum;
        this.tileTypeDecidingFunction = (fraction) -> {
            if(fraction < primaryToSecondaryBound) {
                return TileType.Primary;
            }
            if(fraction < secondaryToEmptyWeightBound) {
                return TileType.Secondary;
            }
            return TileType.Empty;
        };
        this.tileSize = tileSize;
        this.xCount = xCount;
        this.yCount = yCount;
        this.primaryColor = primary.getRGB();
        this.secondaryColor = secondary.getRGB();
    }


    @Override
    public void draw( PApplet applet, int xOffset, int yOffset )
    {
        this.draw( applet.getGraphics(), xOffset, yOffset );
    }

    @Override
    public void draw( PGraphics graphics, int xOffset, int yOffset )
    {
        final Random random = new Random(this.seed);

        for(int row = 0; row < yCount; row++) {
            for(int column = 0; column < Math.ceil( xCount / 2f ); column++) {
                TileType tileType = this.tileTypeDecidingFunction.invoke( random.nextFloat() );
                if(tileType.equals( TileType.Empty )) {
                    continue;
                }

                graphics.fill( tileType.equals( TileType.Primary ) ? this.primaryColor : this.secondaryColor );

                var offsets = this.getYAndXOffsets( column, row, xOffset, yOffset );


                graphics.rect( offsets.B(), offsets.A(), tileSize, tileSize );
                graphics.rect( offsets.C(), offsets.A(), tileSize, tileSize );
            }
        }
    }

    private Tuple.Three<Integer, Integer, Integer> getYAndXOffsets( int column, int row, int xOffset, int yOffset) {
        int columnMirror = xCount - 1 - column;

        int localXOffset = column * this.tileSize;
        int localXMirrorOffset = columnMirror * this.tileSize;

        int localYOffset = row * this.tileSize;

        int worldXOffset = localXOffset + xOffset;
        int worldXMirroredOffset = localXMirrorOffset + xOffset;

        int worldYOffset = localYOffset + yOffset;

        return new Tuple.Three<>( worldYOffset, worldXOffset, worldXMirroredOffset );
    }
}
