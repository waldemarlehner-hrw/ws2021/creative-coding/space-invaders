package de.waldemarlehner.spaceinvaders;

import de.waldemarlehner.javaListenerUtils.Tuple;
import processing.core.PApplet;
import processing.core.PImage;

import java.awt.*;
import java.util.Random;

public class Main extends PApplet
{
    /// Settings
    private final int pixelsFromSourcePerInvader = 8;
    private final int tilesPerInvader = 13;
    private final int pixelsPerTile = 4;
    private final int gap = 3;
    private int seed = 0;


    private final int emptyWeight = 20;
    private final int primaryWeight = 5;
    private final int secondaryWeight = 5;

    /// end

    int xCount;
    int yCount;
    private PImage source;
    private final Random colorRandom = new Random(seed);


    @Override
    public void settings() {
        super.smooth(0);
        this.source = super.loadImage( "input.png" );
        this.xCount = source.width / this.pixelsFromSourcePerInvader;
        this.yCount = source.height / this.pixelsFromSourcePerInvader;
        super.size( xCount * (tilesPerInvader * pixelsPerTile + gap) - gap, yCount * (tilesPerInvader * pixelsPerTile + gap) - gap);
    }



    @Override
    public void setup()
    {
        super.noStroke();
        super.background( 0x000000 );
        Tuple.Two<Color, Color>[][] secondaryAndPrimaryColorsSet = this.generateColors(source);

        IDrawable[][] invaders = this.buildInvaders(secondaryAndPrimaryColorsSet);

        int xCount = invaders.length;
        int yCount = invaders[0].length;

        super.fill( 0xFF0000 );
        super.rect( 0, 0, 100, 100 );

        for(int x = 0; x < xCount; x++) {
            for (int y = 0; y < yCount; y++) {
                int xOffset = x * (tilesPerInvader * pixelsPerTile + gap);
                int yOffset = y * (tilesPerInvader * pixelsPerTile + gap);
                invaders[x][y].draw( this, xOffset, yOffset );
            }
        }

        super.save( "output.png" );
        System.out.println("Done");
        System.exit( 0 );


    }

    private IDrawable[][] buildInvaders( Tuple.Two<Color, Color>[][] secondaryAndPrimaryColorsSet )
    {
        int xCount = secondaryAndPrimaryColorsSet.length;
        int yCount = secondaryAndPrimaryColorsSet[0].length;
        IDrawable[][] returnArray = new IDrawable[xCount][yCount];
        for(int x = 0; x < xCount; x++) {
            for(int y = 0; y < yCount; y++) {
                Tuple.Two<Color, Color> color = secondaryAndPrimaryColorsSet[x][y];

                returnArray[x][y] = new SpaceInvader( this.emptyWeight, this.primaryWeight, this.secondaryWeight, color.A(), color.B(), tilesPerInvader, tilesPerInvader, seed++, pixelsPerTile );
            }
        }
        return returnArray;
    }

    private Tuple.Two<Color, Color>[][] generateColors( PImage source )
    {

        Tuple.Two<Color, Color>[][] arr = new Tuple.Two[xCount][yCount];

        for( int x = 0; x < xCount; x++) {
            for( int y = 0; y < yCount; y++) {
                int lowerX = x * this.pixelsFromSourcePerInvader;
                int lowerY = y * this.pixelsFromSourcePerInvader;
                Tuple.Two<Color, Color> primaryAndSecondary = this.getPrimaryAndSecondaryColorRGBRandom(lowerX, lowerY, source);
                arr[x][y] = primaryAndSecondary;
            }
        }

        return arr;
    }

    private Tuple.Two<Color, Color> getPrimaryAndSecondaryColorRGBRandom(int lowerX, int lowerY, PImage source) {
        int upperX = lowerX + this.pixelsFromSourcePerInvader;
        int upperY = lowerY + this.pixelsFromSourcePerInvader;

        int x1 = (int)(this.colorRandom.nextFloat() * (upperX - lowerX) + lowerX);
        int x2 = (int)(this.colorRandom.nextFloat() * (upperX - lowerX) + lowerX);
        int y1 = (int)(this.colorRandom.nextFloat() * (upperY - lowerY) + lowerY);
        int y2 = (int)(this.colorRandom.nextFloat() * (upperY - lowerY) + lowerY);

        Color primary = new Color( source.get(x1, y1) );
        Color secondary = new Color( source.get(x2, y2) );

        return new Tuple.Two<>( primary, secondary );

    }

    private Tuple.Two<Color, Color> getPrimaryAndSecondaryColorHSVAverage( int lowerX, int lowerY, PImage source )
    {
        float primaryH = 0;
        float primaryS = 0;
        float primaryV = 0;
        float secondaryH = 0;
        float secondaryS = 0;
        float secondaryV = 0;

        int primaryPixelCount = 0;
        int secondaryPixelCount = 0;
        float[] hsv = new float[3];
        for(int x = lowerX; x < lowerX + this.pixelsFromSourcePerInvader; x++) {
            for(int y = lowerY; y < lowerY + this.pixelsFromSourcePerInvader; y++) {
                Color c = new Color( source.get(x,y) );
                hsv = Color.RGBtoHSB( c.getRed(), c.getGreen(), c.getBlue(), hsv );
                if(x+y % 2 == 0) {
                    primaryH += hsv[0];
                    primaryS += hsv[1];
                    primaryV += hsv[2];
                    primaryPixelCount++;
                }
                else {
                    secondaryH += hsv[0];
                    secondaryS += hsv[1];
                    secondaryV += hsv[2];
                    secondaryPixelCount++;
                }
            }
        }

        if(this.pixelsFromSourcePerInvader == 1) {
            primaryH += secondaryH;
            primaryS += secondaryS;
            primaryV += secondaryV;
            primaryPixelCount += secondaryPixelCount;
        }

        Color primary = Main.buildColorFromHSVSums( primaryH * 360, primaryS * 100, primaryV * 100, primaryPixelCount );
        if(this.pixelsFromSourcePerInvader == 1) {
            return new Tuple.Two<>( primary, primary );
        }
        Color secondary = Main.buildColorFromHSVSums( secondaryH, secondaryS, secondaryV, secondaryPixelCount );
        return new Tuple.Two<>( primary, secondary );

    }

    private static Color buildColorFromHSVSums(float hSum, float sSum, float vSum, int count) {
        float h = hSum / count;
        float s = sSum / count;
        float v = vSum / count;

        return Color.getHSBColor( h,s,v );
    }

    // Program entry point
    public static void main( String[] args )
    {
        PApplet.main( Main.class, args );
    }

}
