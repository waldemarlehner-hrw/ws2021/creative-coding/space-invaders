# Java Processing Template

This repository is an example of how to make use of the Processing Framework directly from Java.
This is especially helpful for bigger projects,
especially if you want to use a more advanced IDE than the one provided by Processing.

## Prerequisites
This template uses maven as its build tool.  
Make sure that maven is installed.

## Structure
This project is set up using the usual Maven Project Structure.
You have your source files under `src/main/java/your/project/namespace/...`

Build files will appear under `target/`

## Building Project
To build the project, run `mvn install`.
To bundle the project into an executable jar, run `mvn bundle`.

Please note that you need to modify the `pom.xml` when changing file names. (mainly the mainClass property.)

## CI/CD
There is a `.gitlab-ci.yml` File in this repository. This file defines an automatic build and release procedure.
It will build the project whenever a push on main occurs, and will create a release for every tagged commit.

This will only work if you are using Gitlab. Feel free to delete the file if you do not need automatic builds.
